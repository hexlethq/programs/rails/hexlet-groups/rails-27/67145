# frozen_string_literal: true

# BEGIN
def compare_versions(version_a, version_b)
  a_nums = version_a.split('.').map(&:to_i)
  b_nums = version_b.split('.').map(&:to_i)
  zip_nums = a_nums.zip(b_nums)
  zip_nums.each do |pair|
    version_a, version_b = pair
    compare_res = version_a <=> version_b
    return compare_res if compare_res != 0
  end
  0
end
# END
