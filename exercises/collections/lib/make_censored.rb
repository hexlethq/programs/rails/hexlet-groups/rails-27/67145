# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  replacement = '$#%!'
  words = text.split
  words.map do |word|
    stop_words.include?(word) ? replacement : word
  end.join(' ')
  # END
end
