# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  arr = params.to_a.sort
  parts = arr.map do |(key, value)|
    "#{key}=#{value}"
  end
  parts.join('&')
end
# END
