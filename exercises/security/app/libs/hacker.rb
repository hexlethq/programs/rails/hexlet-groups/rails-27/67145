# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      sign_up_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users/sign_up')
      post_form_uri = URI('https://rails-l4-collective-blog.herokuapp.com/users')

      response = URI.open(sign_up_uri)

      doc = Nokogiri::HTML(response)
      meta = doc.css('input[name="authenticity_token"]')
      csrf_token = meta.first.attributes['value'].value
      cookie = response.meta['set-cookie'].split(';')[0]

      params = {
        authenticity_token: csrf_token,
        user: { email: email, password: password, password_confirmation: password }
      }

      headers = {
        Cookie: cookie
      }

      Net::HTTP.post(post_form_uri, URI.encode_www_form(params), headers)
    end
    # END
  end
end
