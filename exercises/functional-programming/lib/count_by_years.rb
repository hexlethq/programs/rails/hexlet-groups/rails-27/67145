# frozen_string_literal: true

# BEGIN
require 'date'
def count_by_years(users)
  users_with_year = users.map do |user|
    user[:year] = Date.parse(user[:birthday]).year
    user
  end
  male_users = users_with_year.filter { |user| user[:gender] == 'male' }
  male_users.each_with_object({}) do |user, hash|
    year = user[:year].to_s
    hash[year] = hash.fetch(year, 0) + 1
  end
end
# END
