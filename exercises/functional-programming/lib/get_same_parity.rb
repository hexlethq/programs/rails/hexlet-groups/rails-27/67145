# frozen_string_literal: true

# BEGIN
def get_same_parity(nums)
  return nums if nums.count.zero?

  first_parity = nums.first % 2
  nums.filter do |num|
    num % 2 == first_parity
  end
end
# END
