# frozen_string_literal: true

# BEGIN
def anagramm_filter(target, words)
  sorted_target = target.chars.sort.join
  words.filter do |word|
    word.chars.sort.join == sorted_target
  end
end
# END
