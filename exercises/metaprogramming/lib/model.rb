# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.send :include, InstanceMethods
    base.extend ClassMethods
  end

  module ClassMethods
    def config
      @config ||= {}
    end

    def attribute(name, options = {})
      config[name] = options

      define_method name do
        attributes[name]
      end

      define_method "#{name}=" do |value|
        attributes[name] = convert(value, self.class.config[name][:type])
      end
    end

    def default(name)
      config[name][:default]
    end
  end

  module InstanceMethods
    attr_reader :attributes

    def initialize(attributes)
      @attributes = self.class.config.each_with_object({}) do |entries, obj|
        key, value = entries
        obj[key] = convert(attributes.fetch(key, value[:default]), value[:type])
      end
    end

    private

    def convert(value, type)
      converters = {
        string: ->(val) { val.to_s },
        integer: ->(val) { val.to_i },
        datetime: ->(val) { DateTime.parse(val) },
        boolean: lambda do |val|
          return true if ['yes', true].include? val
          return false if ['no', false].include? val

          val.to_bool
        end
      }
      value.nil? ? value : converters[type].call(value)
    end
  end
end
# END
