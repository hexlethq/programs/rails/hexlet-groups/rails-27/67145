# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    attributes = { active: true }
    result_attributes = { name: 'Andrey', birthday: nil, active: true }

    user = User.new(attributes)
    assert_equal result_attributes, user.attributes
  end
  # END
end
