# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homes#index'

  # BEGIN
  resources :posts do
    resources :comments, module: :posts
  end
  # END

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
