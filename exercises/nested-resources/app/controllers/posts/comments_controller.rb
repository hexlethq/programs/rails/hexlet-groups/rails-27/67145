# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :set_post
    before_action :set_comment, only: %i[edit update destroy]

    def index
      @comments = @post.comments
    end

    def show; end

    def new
      @comment = @post.comments.build
    end

    def edit; end

    def create
      @comment = @post.comments.build(comment_params)

      if @comment.save
        redirect_to @post, notice: 'Comment was successfully created.'
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @comment.update(comment_params)
        redirect_to @comment.post, notice: 'Comment was successfully updated.'
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @comment.destroy
      redirect_to @post, notice: 'Comment was successfully destroyed.'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Post::Comment.find(params[:id])
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

    # Only allow a list of trusted parameters through.
    def comment_params
      params.require(:post_comment).permit(:body)
    end
  end
end
