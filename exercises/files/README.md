# Файлы

работа со статьями, создавать может только залогиненный, редактировать только автор, удалять только админ

## Ссылки

* Документация [Active Storage](https://edgeguides.rubyonrails.org/active_storage_overview.html)
* Инструкция [S3 для Heroku](https://devcenter.heroku.com/articles/s3)
* [file_validators](https://github.com/musaffa/file_validators) — валидатор файлов

## Задачи

### Gemfile

Добавьте необходимые гемы и выполните установку

```ruby
gem 'active_storage_validations'
gem 'aws-sdk-s3'
gem 'image_processing'
```

Создайте миграцию для ActiveStorage.

```bash
bin/rails active_storage:install
```

## app/models/vehicle.rb

Добавьте в модель связь `image` с загружаемым изображением.

## config/storage.yml

Добавьте в *config/storage.yml* конфиг для работы с файлами. При локальной работе файлы будут храниться на диске, а в боевом окружении - на AWS S3.

```yml
test:
  service: Disk
  root: <%= Rails.root.join("tmp/storage") %>

local:
  service: Disk
  root: <%= Rails.root.join("storage") %>

amazon:
  service: S3
  access_key_id: <%= ENV['AWS_ACCESS_KEY_ID'] %>
  secret_access_key: <%= ENV['AWS_SECRET_ACCESS_KEY'] %>
  region: <%= ENV['AWS_REGION'] %>
  bucket: <%= ENV['AWS_BUCKET'] %>
  ```

### config/environments/production.rb

Установите использование Amazon для хранения в продакшен окружении:

```ruby
config.active_storage.service = :amazon
```

### app/views/vehicles/_form.html.slim

Добавьте в форму возможность загружать изображения.

### app/models/vehicle.rb

Добавьте валидацию для загрузки изображения. Изображения должно быть обязательным, размер - до 5 мегабайт. Разрешенные форматы — png, jpg, jpeg.

### AWS S3

Зарегистрируйтесь на Amazon Web Services. Создайте новый бакет, создайте секретные токены n *Access Key ID* и *Secret Access Key*.

Для работы с AWS S3 приложению необходимы следующие переменные окружения

```env
SECRET_KEY_BASE=test
RAILS_ENV=production # для запуска приложения в production режиме
RACK_ENV=production
AWS_ACCESS_KEY_ID= # Получено в AWS
AWS_BUCKET= # например rails-files-homework
AWS_REGION= # например eu-central-1
AWS_SECRET_ACCESS_KEY= # Получено в AWS
```

Выполните деплой приложение на Heroku или запуститп прилложение в режиме production локально и проверьте загрузку файлов на Amazon S3 (файлы должны отобразиться на AWS и быть доступны в приложении).
