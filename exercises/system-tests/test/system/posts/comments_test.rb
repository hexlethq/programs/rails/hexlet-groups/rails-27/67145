# frozen_string_literal: true

require 'application_system_test_case'

class PostCommentsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    visit post_url(id: @post)
  end

  test 'should create new comment' do
    content = 'This is the comment content'

    fill_in 'post_comment_body', with: content

    click_on 'Create Comment'

    assert_text content
  end

  test('should edit comment') do
    new_content = 'This is updated content'

    click_on 'Edit', match: :first

    fill_in 'Body', with: new_content

    click_on 'Update Comment'

    assert_text new_content
  end

  test('should delete comment') do
    list = all('a', text: 'Delete')

    assert_equal 1, list.size

    page.accept_confirm do
      click_on 'Delete', match: :first
    end

    list = all('a', text: 'Delete')
    assert list.size.zero?
  end
end
