# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
  end

  test 'viewing the index' do
    visit posts_url
    assert_selector 'h1', text: 'Posts'
  end

  test 'should create new post' do
    title = 'Creating an Post'
    visit posts_url

    click_on 'New Post'

    fill_in 'Title', with: title
    fill_in 'Body', with: 'Created this post successfully!'

    click_on 'Create Post'

    assert_text title
  end

  test 'should edit post' do
    visit posts_url
    click_on 'Edit', match: :first

    fill_in 'Title', with: @post.title
    click_on 'Update Post'

    assert_text 'Post was successfully updated'
  end

  test 'should delete a post' do
    visit posts_url

    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed.'
  end
end
# END
