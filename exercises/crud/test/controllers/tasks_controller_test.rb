# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'index action' do
    get tasks_url
    assert_response :success
  end

  test 'show action' do
    get task_url(tasks(:task1))
    assert_response :success
  end

  test 'new action' do
    get new_task_url
    assert_response :success
  end

  test 'edit action' do
    task = tasks(:task1)
    get edit_task_url(task)
    assert_response 200
  end

  test 'should create task' do
    assert_difference('Task.count') do
      post tasks_url,
           params: { task: { name: 'Task 1',
                             description: 'Description 1',
                             status: 'New',
                             creator: 'Me',
                             performer: 'He',
                             completed: false } }
    end

    assert_redirected_to task_path(Task.last)
  end

  test 'should update task' do
    task = Task.find(tasks(:task1).id)
    expected_name = 'Task updated name'

    patch task_url(task), params: { task: { name: expected_name } }

    assert_equal(Task.find(task.id).name, expected_name)
  end

  # TODO: is it possible to use invalid fixtures in the rails?
  test 'returns 422 status if was unable to create/update task' do
    post tasks_url,
         params: { task: { description: 'Description 1',
                           status: 'New',
                           creator: 'Me',
                           performer: 'He',
                           completed: false } }
    assert_response 422
  end

  test 'should delete task' do
    task = tasks(:task1)
    delete task_url(task.id)
    assert_not(Task.exists?(task.id))
  end

  test 'should flash message if task was not removed' do
    # non existing id
    delete task_url(500)
    assert_equal 'Task was already deleted', flash[:notice]

    # TODO: How to test other errors?
  end
end
