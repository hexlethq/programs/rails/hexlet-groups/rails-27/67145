# frozen_string_literal: true

class TasksController < ApplicationController
  def index
    @tasks = Task.all
  end

  def show
    @task = Task.find(params[:id])
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params.merge({ status: 'new' }))

    if @task.save
      redirect_to @task
    else
      render :new, status: 422
    end
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])

    if @task.update(task_params)
      redirect_to @task
    else
      render :edit, status: 422
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy
    redirect_to root_path
  rescue ActiveRecord::RecordNotFound
    flash[:notice] = 'Task was already deleted'
    redirect_to root_path
  rescue ActiveRecord::RecordNotDestroyed
    flash[:error] = 'An error occurred, task was not removed'
    redirect_to action: 'show', id: params[:id]
  end

  private

  def task_params
    params.require(:task).permit(:name, :description, :performer, :creator, :completed)
  end
end
