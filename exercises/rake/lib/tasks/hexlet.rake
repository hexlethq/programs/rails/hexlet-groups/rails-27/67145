# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'imports users'
  task :import_users, [:filename] => :environment do |_task, args|
    filename = args[:filename]

    CSV.foreach(filename, headers: true) do |row|
      user = row.to_h
      User.create user
    end
  end
end
