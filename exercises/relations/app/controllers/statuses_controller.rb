# frozen_string_literal: true

class StatusesController < ApplicationController
  before_action :set_status, only: %i[show edit update destroy]

  def index
    @statuses = Status.all
  end

  def show; end

  def new
    @status = Status.new
  end

  def edit; end

  def create
    @status = Status.new(status_params)

    if @status.save
      redirect_to @status, notice: 'Status was successfully created.'
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @status.update(status_params)
      redirect_to @status, notice: 'Status was successfully updated.'
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @status.destroy
    redirect_to statuses_url, notice: 'Status was successfully destroyed.'
  rescue ActiveRecord::InvalidForeignKey
    redirect_back fallback_location: root_path, notice: 'Statuses can not be deleted until it is used in the tasks.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_status
    @status = Status.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def status_params
    params.require(:status).permit(:name)
  end
end
