# frozen_string_literal: true

class Router
  def call(env)
    request = Rack::Request.new(env)
    case request.path
    when '/about'
      [200, {}, ['About page']]
    when '/'
      [200, {}, ['Hello, World!']]
    else
      [404, {}, ['404 Not Found']]
    end
  end
end
