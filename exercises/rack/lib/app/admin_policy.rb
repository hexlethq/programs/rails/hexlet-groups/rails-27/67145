# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    request = Rack::Request.new(env)
    return [403, {}, ['Forbidden']] if request.path.start_with?('/admin')

    [status, headers, body]
  end
end
