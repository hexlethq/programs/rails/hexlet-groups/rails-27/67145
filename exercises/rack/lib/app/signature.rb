# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body = @app.call(env)
    body_str = body.join
    encrypted_body = Digest::SHA2.hexdigest body_str
    [status, headers, ["#{body_str}\n#{encrypted_body}"]]
  end
end
