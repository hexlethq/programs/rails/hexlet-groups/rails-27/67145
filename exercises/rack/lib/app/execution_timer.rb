class ExecutionTimer
  def initialize(app)
    @app = app
  end

  def call(env)
    start_time = Time.now
    status, headers, body = @app.call(env)
    end_time = Time.now
    duration_ms = ((end_time - start_time) * 1000).round(3)
    [status, headers, ["#{body.join}\nPage was loading: #{duration_ms} ms"]]
  end
end