# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    locale = extract_locale(env['HTTP_ACCEPT_LANGUAGE'])
    locale = I18n.default_locale unless I18n.available_locales.include?(locale&.to_sym)
    I18n.locale = locale
    status, headers, response = app.call(env)
    headers = Rack::Utils::HeaderHash.new(headers)
    headers['Content-Language'] = locale.to_s unless headers['Content-Language']
    [status, headers, response]
  end

  private

  def extract_locale(locale_header)
    locale_header&.scan(/^[a-z]{2}/)&.first
  end
  # END
end
