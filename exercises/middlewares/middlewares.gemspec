# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.version = '0.1.0'
  spec.name    = 'middlewares'
  spec.summary = 'First Module'
  spec.authors = ['Hexlet']
  spec.files   = `git ls-files`
  spec.required_ruby_version = '>= 3.0.0'
end
