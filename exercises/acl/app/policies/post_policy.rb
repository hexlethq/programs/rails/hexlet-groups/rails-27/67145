# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def create?
    user
  end

  def new?
    create?
  end

  def edit?
    user && (record.author_id == user.id or user.admin?)
  end

  def update?
    edit?
  end

  def destroy?
    user&.admin?
  end
  # END
end
