# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

30.times do |_i|
  post = Post.create!(
    title: Faker::Hipster.sentence(word_count: rand(2..7)),
    body: Faker::Hipster.paragraph(sentence_count: rand(2..5))
  )

  rand(0..5).times do |_|
    post.comments.create(body: Faker::Hipster.paragraph(sentence_count: rand(1..2)))
  end
end
