# frozen_string_literal: true

require 'test_helper'

class EnTranslationTest < ActionDispatch::IntegrationTest
  def setup
    @post = posts :without_comments
  end

  test 'home#index' do
    get root_path(locale: @locale)

    assert_select 'h2', text: 'Home page'
    assert_select 'ul.nav', text: /Home/
    assert_select 'ul.nav', text: /Posts/
    assert_select 'footer', text: /Hexlet, 2021/
  end

  test 'posts#index' do
    get posts_path(locale: :en)

    assert_select 'h2', text: 'Posts'
    assert_select '#new-post-link', text: 'New Post'
  end

  test 'posts#new' do
    get new_post_path(locale: :en)

    assert_select 'h2', text: 'New Post'
    assert_select '.new_post>.btn-success[value=?]', 'Create Post'
    assert_select '.new_post #post_title[placeholder=?]', 'Enter post title'
  end

  test 'posts#create check validation' do
    assert_no_difference 'Post.count' do
      post posts_url(locale: :en), params: { post: {
        title: nil
      } }
    end

    assert_select '.alert-danger', 'Please check the problems:'
  end

  test 'posts#create successfully' do
    post posts_url(locale: :en), params: { post: {
      title: 'Test'
    } }

    follow_redirect!

    assert_select '.alert-info', 'Post was created.'
  end

  test 'posts#update' do
    patch post_url(@post, locale: :en), params: { post: {
      title: 'Title',
      body: 'Body'
    } }

    follow_redirect!

    assert_select '.alert-info', 'Post was updated.'
  end

  test 'posts#destroy' do
    delete post_url(@post, locale: :en)

    follow_redirect!

    assert_select '.alert-info', 'Post was destroyed.'
  end

  test 'comment#create' do
    post post_comments_url(@post, locale: :en), params: { post_comment: {
      body: 'test'
    } }

    follow_redirect!

    assert_select '.alert-info', 'Comment was created.'
  end

  test 'comment#update' do
    comment = post_comments(:one)
    post = comment.post

    patch post_comment_url(post, comment, locale: :en), params: { post_comment: {
      body: 'new body'
    } }

    follow_redirect!

    assert_select '.alert-info', 'Comment was updated.'
  end
end
