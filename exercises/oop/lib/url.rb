# frozen_string_literal: true

require('forwardable')
require('uri')

# BEGIN
class Url
  QUERY_DELIMITER = '&'
  PARAMS_DELIMITER = '='

  extend Forwardable
  def_delegators :@url, :scheme, :host, :to_s

  include Comparable

  def initialize(url)
    @url = URI(url)
  end

  def query_params
    query = @url.query
    query.split(QUERY_DELIMITER).each_with_object({}) do |param, map|
      key, value = param.split(PARAMS_DELIMITER)
      map[key.to_sym] = value
    end
  end

  def query_param(key, default_value = nil)
    query_params.fetch(key, default_value)
  end

  def <=>(other)
    @url.to_s <=> other.to_s
  end
end
# END
