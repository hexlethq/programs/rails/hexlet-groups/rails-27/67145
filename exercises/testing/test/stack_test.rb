# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_initialized_default_empty
    stack = Stack.new
    assert(stack.empty? == true)
  end

  def test_initialized_with_elements
    initial = [1, 2]
    stack = Stack.new(initial.dup)
    assert(stack.to_a == initial)
  end

  def test_pop_method
    initial = [1, 2]
    stack = Stack.new(initial.dup)
    assert(!stack.empty?)
    assert(stack.size == initial.size)
    assert(stack.pop! == initial.pop)
    assert(stack.pop! == initial.pop)
    assert(stack.empty?)
  end

  def test_push_and_to_a
    stack = Stack.new
    stack.push! 'first'
    assert(stack.size == 1)
    stack.push! 'second'
    assert(stack.size == 2)
    assert(stack.to_a == %w[first second])
  end

  def test_clear
    stack = Stack.new [1, 2, 3]
    assert(stack.size == 3)
    stack.clear!
    assert(stack.empty?)
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
