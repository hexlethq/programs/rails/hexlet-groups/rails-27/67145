# frozen_string_literal: true

# BEGIN
def reverse(str)
  # it would be too easy
  # str.reverse
  res = ''
  (0..str.length - 1).each do |i|
    res += str[str.length - 1 - i]
  end
  res
end
# END
