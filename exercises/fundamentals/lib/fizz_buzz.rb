# frozen_string_literal: true

# BEGIN
def generate_string(num)
  dict = { 3 => 'Fizz', 5 => 'Buzz' }
  res = ''
  dict.each do |key, value|
    res += value if (num % key).zero?
  end
  res.empty? ? num.to_s : res
end

def fizz_buzz(start, stop)
  res = []
  (start..stop).each do |num|
    res << generate_string(num)
  end
  res.join(' ')
end
# END
